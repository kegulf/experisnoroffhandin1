﻿using System;

namespace Introday
{
    class IntroDay
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello! Which task would you like to run?" +
                "\n\t1. Task 0 - Hello World" +
                "\n\t2. Task 1 - Hello lecturers" +
                "\n\t3. Task 2 - Basic input" +
                "\n\t4. Exit program");

            int usersChoiceOfTask = Convert.ToInt32(Console.ReadLine());


            switch(usersChoiceOfTask)
            {
                case 1:
                    HelloWorld();
                    break;
                case 2:
                    HelloLecturers();
                    break;
                case 3:
                    BasicInput();
                    break;
                case 4:
                    ExitProgram();
                    break;
                default:
                    Console.WriteLine("\nPlease choose a valid input (1-4)");
                    Main(null);
                    break;
            }

            string usersChoiceToRetry = "";
            while (!(usersChoiceToRetry.Equals("y") || usersChoiceToRetry.Equals("n")))
            {
                Console.WriteLine("\n\nDo you want to retry? (y/n)");
                usersChoiceToRetry = Console.ReadLine();
            }

            if(usersChoiceToRetry.Equals("y"))
            {
                Main(null);
            } else
            {
                ExitProgram();
            }


        }

        /**
         *  Basic HelloWorld method
         */
        static void HelloWorld()
        {
            Console.WriteLine("\nHello World!");
        }

        /** 
         *  Task 1, printing a string that isn't "Hello World" :O
         */
        static void HelloLecturers()
        {
            Console.WriteLine("\nHello Dean and Gregg.\nMy name is Odd, and I'm quite odd. *Badum tshh*");

        }

        /**
         *  Task 2, testing out basic input.
         */
        static void BasicInput()
        {
            Console.WriteLine("\nHello there, what's your name?");
            string name = Console.ReadLine();
            string spacelessName = name.Replace(" ", "");

            Console.WriteLine("\nHello " + name);
            Console.WriteLine("Your name is " + spacelessName.Length + " characters long.");

            if (spacelessName.Length != name.Length)
            {
                Console.WriteLine("If you count the spaces it's " + name.Length + " characters long");
            }

            Console.WriteLine("The first character in your name is " + name.Substring(0, 1));
        }

        static void ExitProgram()
        {
            Console.WriteLine("\nThank you for choosing an Odd service! Bye now");
            Environment.Exit(0);
        }
    }
        
}
